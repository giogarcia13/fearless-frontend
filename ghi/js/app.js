window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl,starts,ends, locationName) {
      return `
      <div class="d-flex align-items-start flex-column mb-3">
        <div class="card shadow-lg p-3 mb-5 bg-white rounded">
              <img src="${pictureUrl}" class="card-img-top">
              <div class="card-body">
                  <h5 class="card-title">${name}</h5>
                  <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                  <p class="card-text">${description}</p>
              </div>
              <div class="card-footer">
                  <small class="text-muted">${starts} - ${ends}</small>
              </div>
          </div>
          `;
      }

      const url = 'http://localhost:8000/api/conferences/';

      try {
          const response = await fetch(url);

          if (!response.ok) {
              console.error("bad response");
          } else {
          const data = await response.json();

          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const startsDate = new Date(details.conference.starts);
                  const starts = startsDate.getMonth() + "/" + startsDate.getDate() + "/" + startsDate.getFullYear()
                  const endsDate = new Date(details.conference.ends);
                  const ends = endsDate.getMonth() + "/" + endsDate.getDate() + "/" + endsDate.getFullYear()
                  const locationName = details.conference.location.name
                  const html = createCard(name, description, pictureUrl,starts,ends,locationName);
                  const column = document.querySelector('.col');
                  column.innerHTML += html;
              }
          }

          }
      } catch (e) {
          console.error(e);
      }

      });
